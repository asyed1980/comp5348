﻿using System; 

namespace VideoStore.Services.MessageTypes
{
    public class NotificationMessage
    {
        public enum Status { Success, Failure };
        public enum Type { Delivery, Transfer }
        public string Message { get; set; }
        public string orderRef { get; set; }
        public  Type NotificationType { get; set; }
        public Status NotificationStatus { get; set; }

    }
}
