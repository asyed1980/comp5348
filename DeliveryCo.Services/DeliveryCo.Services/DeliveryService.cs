﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DeliveryCo.Services.Interfaces;
using DeliveryCo.Business.Components.Interfaces;
using System.ServiceModel;
using Microsoft.Practices.ServiceLocation;
using DeliveryCo.MessageTypes;
using System.Threading;
using DeliveryCo.Services.DeliveryNotficationService;

namespace DeliveryCo.Services
{
   
    public class DeliveryService : IDeliveryService
    {
        public enum DeliveryStatus { Submitted, DELIVERED, FAILED }
        INotificationService notficationServiceProxy = new NotificationServiceClient();


        private IDeliveryProvider DeliveryProvider
        {
            get
            {
                return ServiceLocator.Current.GetInstance<IDeliveryProvider>();
            }
        }

        [OperationBehavior(TransactionScopeRequired = true)]
        public void SubmitDelivery(DeliveryInfo pDeliveryInfo)
        {// our QueMessage reaches here and later processed by this system  

            pDeliveryInfo.Status = (int)DeliveryStatus.Submitted;
            DeliveryProvider.SubmitDelivery(
            MessageTypeConverter.Instance.Convert<DeliveryCo.MessageTypes.DeliveryInfo,
            DeliveryCo.Business.Entities.DeliveryInfo>(pDeliveryInfo)
        );
            
            simulateDeliveryJob(pDeliveryInfo.DeliveryIdentifier);
        }
        
        private void simulateDeliveryJob(Guid pDeliveryInfo) {
            System.Threading.Thread.Sleep(3000);
            ThreadPool.QueueUserWorkItem(new WaitCallback((pObj) => DeliveryProvider.ScheduleDelivery(pDeliveryInfo)));
            NotificationMessage msg = new NotificationMessage();
            msg.NotificationStatus = NotificationMessage.Status.Success;
            msg.NotificationType = NotificationMessage.Type.Delivery;
            msg.orderRef = pDeliveryInfo.ToString();
            msg.Message = "Successful delivery : deliveryReference :"+pDeliveryInfo;
            notficationServiceProxy.Notification(msg);
        }
    }
}
