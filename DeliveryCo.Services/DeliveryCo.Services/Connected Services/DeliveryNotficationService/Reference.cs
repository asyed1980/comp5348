﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DeliveryCo.Services.DeliveryNotficationService {
    using System.Runtime.Serialization;
    using System;
    
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="NotificationMessage", Namespace="http://schemas.datacontract.org/2004/07/VideoStore.Services.MessageTypes")]
    [System.SerializableAttribute()]
    public partial class NotificationMessage : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string MessageField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private DeliveryCo.Services.DeliveryNotficationService.NotificationMessage.Status NotificationStatusField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private DeliveryCo.Services.DeliveryNotficationService.NotificationMessage.Type NotificationTypeField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string orderRefField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Message {
            get {
                return this.MessageField;
            }
            set {
                if ((object.ReferenceEquals(this.MessageField, value) != true)) {
                    this.MessageField = value;
                    this.RaisePropertyChanged("Message");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public DeliveryCo.Services.DeliveryNotficationService.NotificationMessage.Status NotificationStatus {
            get {
                return this.NotificationStatusField;
            }
            set {
                if ((this.NotificationStatusField.Equals(value) != true)) {
                    this.NotificationStatusField = value;
                    this.RaisePropertyChanged("NotificationStatus");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public DeliveryCo.Services.DeliveryNotficationService.NotificationMessage.Type NotificationType {
            get {
                return this.NotificationTypeField;
            }
            set {
                if ((this.NotificationTypeField.Equals(value) != true)) {
                    this.NotificationTypeField = value;
                    this.RaisePropertyChanged("NotificationType");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string orderRef {
            get {
                return this.orderRefField;
            }
            set {
                if ((object.ReferenceEquals(this.orderRefField, value) != true)) {
                    this.orderRefField = value;
                    this.RaisePropertyChanged("orderRef");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
        
        [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
        [System.Runtime.Serialization.DataContractAttribute(Name="NotificationMessage.Status", Namespace="http://schemas.datacontract.org/2004/07/VideoStore.Services.MessageTypes")]
        public enum Status : int {
            
            [System.Runtime.Serialization.EnumMemberAttribute()]
            Success = 0,
            
            [System.Runtime.Serialization.EnumMemberAttribute()]
            Failure = 1,
        }
        
        [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
        [System.Runtime.Serialization.DataContractAttribute(Name="NotificationMessage.Type", Namespace="http://schemas.datacontract.org/2004/07/VideoStore.Services.MessageTypes")]
        public enum Type : int {
            
            [System.Runtime.Serialization.EnumMemberAttribute()]
            Delivery = 0,
            
            [System.Runtime.Serialization.EnumMemberAttribute()]
            Transfer = 1,
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="DeliveryNotficationService.INotificationService")]
    public interface INotificationService {
        
        [System.ServiceModel.OperationContractAttribute(IsOneWay=true, Action="http://tempuri.org/INotificationService/Notification")]
        void Notification(DeliveryCo.Services.DeliveryNotficationService.NotificationMessage msg);
        
        [System.ServiceModel.OperationContractAttribute(IsOneWay=true, Action="http://tempuri.org/INotificationService/Notification")]
        System.Threading.Tasks.Task NotificationAsync(DeliveryCo.Services.DeliveryNotficationService.NotificationMessage msg);
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface INotificationServiceChannel : DeliveryCo.Services.DeliveryNotficationService.INotificationService, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class NotificationServiceClient : System.ServiceModel.ClientBase<DeliveryCo.Services.DeliveryNotficationService.INotificationService>, DeliveryCo.Services.DeliveryNotficationService.INotificationService {
        
        public NotificationServiceClient() {
        }
        
        public NotificationServiceClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public NotificationServiceClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public NotificationServiceClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public NotificationServiceClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public void Notification(DeliveryCo.Services.DeliveryNotficationService.NotificationMessage msg) {
            base.Channel.Notification(msg);
        }
        
        public System.Threading.Tasks.Task NotificationAsync(DeliveryCo.Services.DeliveryNotficationService.NotificationMessage msg) {
            return base.Channel.NotificationAsync(msg);
        }
    }
}
