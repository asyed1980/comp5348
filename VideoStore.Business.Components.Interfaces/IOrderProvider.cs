﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VideoStore.Business.Entities;
using VideoStore.Services.MessageTypes;

namespace VideoStore.Business.Components.Interfaces
{
    public interface IOrderProvider
    {
        VideoStore.Services.MessageTypes.Order SubmitOrder(VideoStore.Business.Entities.Order Order);
        void ProcessBankNotificationFailure(String reference);
        void ProcessBankNotificationSuccess(String reference);
        VideoStore.Business.Entities.Order RetrieveOrder(string Id);
    }
}
