﻿using System; 
using System.Configuration; 
using System.Messaging;
using System.ServiceModel.Configuration; 

namespace VideoStore.Business.Components
{
    public static class QueuedServiceHelper
    {
        public static void VerifyQueues()
        {
            Configuration config = ConfigurationManager.OpenExeConfiguration(
            ConfigurationUserLevel.None);
            ServiceModelSectionGroup sectionGroup =
            ServiceModelSectionGroup.GetSectionGroup(config);
            foreach (ChannelEndpointElement endpointElement in
            sectionGroup.Client.Endpoints)
            {
                if (endpointElement.Binding == "netMsmqBinding")
                {
                    string queue = GetQueueFromUri(endpointElement.Address);
              
                if (MessageQueue.Exists(queue) == false)
                {
                    MessageQueue.Create(queue, true);
                }
                }
            }
        } 

    private static string GetQueueFromUri(Uri uri)
    { 
                string queue = String.Empty;

                
                if (uri.Segments[1] == @"private/")
                {
                    queue = @".\private$\" + uri.Segments[2];
                }
                else
                {
                    queue = uri.Host;
                    foreach (string segment in uri.Segments)
                    {
                        if (segment == "/")
                        {
                            continue;
                        }
                        string localSegment = segment;
                        if (segment[segment.Length - 1] == '/')
                        {
                            localSegment = segment.Remove(segment.Length - 1);
                        }
                        queue += @"\";
                        queue += localSegment;
                    }
                }
                return queue;
            }
        }
}
 
