﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VideoStore.Business.Components.Interfaces;
using VideoStore.Business.Entities;
using System.Transactions;
using Microsoft.Practices.ServiceLocation;
using System.Data.Objects;

namespace VideoStore.Business.Components
{
    public class OrderProvider : IOrderProvider
    {
        public IEmailProvider EmailProvider
        {
            get { return ServiceLocator.Current.GetInstance<IEmailProvider>(); }
        }

        public IUserProvider UserProvider
        {
            get { return ServiceLocator.Current.GetInstance<IUserProvider>(); }
        }


        public VideoStore.Services.MessageTypes.Order SubmitOrder(Entities.Order order)
        {
            using (TransactionScope scope = new TransactionScope())
            {
                LoadMediaStocks(order);
                MarkAppropriateUnchangedAssociations(order);
                using (VideoStoreEntityModelContainer ctx = new VideoStoreEntityModelContainer())
                {
                    try
                    { 
                        int count = (from o in ctx.Orders select o.Id).Count();
                        if (count == 0)
                        {
                            order.Id = 1;
                        }
                        else
                        {
                            order.Id = (from o in ctx.Orders select o.Id).Max() + 1;
                        }   
                        order.OrderNumber = new Guid(order.Id.ToString().PadLeft(32, '0'));
                        order.LoadAndUpdateStockLevels();
                        ctx.Orders.ApplyChanges(order);
                        
                        foreach (OrderItem item in order.OrderItems)
                        {
                            Console.WriteLine("item.Media.ID =" + item.Media.Id);
                            Console.WriteLine("item.Media.Id.ChangeTracker.State =" + item.Media.ChangeTracker.State);
                            Console.WriteLine("item.Media.Stocks.ChangeTracker.State =" + item.Media.Stocks.ChangeTracker.State);

                        } 
                        //stock update

                        ctx.SaveChanges();
                        scope.Complete();
                        ctx.Dispose();
                    }
                    catch (Exception e)
                    {
                        Console.Write(e);
                        scope.Dispose();

                        throw e;
                    }
                }
                return Services.MessageTypeConverter.Instance.Convert<
                        VideoStore.Business.Entities.Order,
                        VideoStore.Services.MessageTypes.Order>(order);

            }
        }


        private void MarkAppropriateUnchangedAssociations(Order pOrder)
        {
            pOrder.Customer.MarkAsUnchanged();
            pOrder.Customer.LoginCredential.MarkAsUnchanged();
            foreach (OrderItem lOrder in pOrder.OrderItems)
            {
                lOrder.Media.Stocks.MarkAsUnchanged();
                lOrder.Media.MarkAsUnchanged();
            }
        }

        private void LoadMediaStocks(Order pOrder)
        {
            using (VideoStoreEntityModelContainer lContainer = new VideoStoreEntityModelContainer())
            {
                foreach (OrderItem lOrder in pOrder.OrderItems)
                {
                    lOrder.Media.Stocks = lContainer.Stocks.Where((pStock) => pStock.Media.Id == lOrder.Media.Id).FirstOrDefault();
                }
            }
        }


        public void ProcessBankNotificationFailure(String reference)
        {
            Order lAffectedOrder = RetrieveOrder(reference);

            using (TransactionScope lScope = new TransactionScope())
            using (VideoStoreEntityModelContainer lContainer = new VideoStoreEntityModelContainer())
            {
                Console.WriteLine("Rolling Back order:" + reference + "....");
                lAffectedOrder.RollbackStockLevels();
                lAffectedOrder.OrderNumber = new Guid("0".PadLeft(32, '0')); // 0 indicates cancelled order 
                lContainer.Orders.Attach(lAffectedOrder);
                lContainer.ObjectStateManager.ChangeObjectState(lAffectedOrder, System.Data.EntityState.Modified);

                foreach (OrderItem lItem in lAffectedOrder.OrderItems)
                {
                    lContainer.Stocks.Attach(lItem.Media.Stocks);
                    lContainer.ObjectStateManager.ChangeObjectState(lItem.Media.Stocks, System.Data.EntityState.Modified);
                }
                lContainer.SaveChanges();
                lScope.Complete();
                Console.WriteLine("Roll Back order:" + reference + " Completed");
            }


        }

        public void ProcessBankNotificationSuccess(String reference)
        {
            Order lAffectedOrder = RetrieveOrder(reference);
            using (TransactionScope lScope = new TransactionScope())
            using (VideoStoreEntityModelContainer lContainer = new VideoStoreEntityModelContainer())
            {
                try
                {
                    //
                    lContainer.Orders.ApplyChanges(lAffectedOrder);
                    lContainer.SaveChanges();
                    lScope.Complete();
                }
                catch (Exception lException)
                {
                    throw lException;
                }
            }
        }




        public Order RetrieveOrder(string Id)
        {

            if (Id.Length < 31)
            {
                //make it guid
                Id = Id.PadLeft(32, '0').ToString();
            }

            Guid orderNum = new Guid(Id);


            using (VideoStoreEntityModelContainer lContainer = new VideoStoreEntityModelContainer())
            {
                Order lOrder = lContainer.Orders.Include("Customer").Include("OrderItems.Media.Stocks").Where((pOrder) => pOrder.OrderNumber == orderNum).FirstOrDefault();
                return lOrder;
            }
        }




    }
}
