﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VideoStore.Business.Entities
{
    public class Test
    {

        static void Main(string[] args) {
            try
            {
                VideoStoreEntityModelContainer ctx = new VideoStoreEntityModelContainer();
                 
                 
                Order order = new Order();
                order.Id = 100;
                order.OrderDate = DateTime.Now;
                order.Total = 50;
                order.Customer  = ctx.Users.Where((u)=>u.Id==2).FirstOrDefault();
                
                
                 

                ctx.Orders.Attach(order);                
                ctx.ObjectStateManager.ChangeObjectState( order, System.Data.EntityState.Added);
                ctx.SaveChanges(); 
                var orders = from o in ctx.Orders
                                select o;
                foreach (var Order in orders){
                    Console.WriteLine("ORDER ID={0}  ORDER TOTAL={1}",
                    Order.Id, Order.Total);
                }

                
            }
            catch (Exception e)
            {
                Console.Write(e.ToString());  
            }
            Console.WriteLine("press any key...........");
            Console.ReadKey();
        }
    }
}
