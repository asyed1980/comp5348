﻿using System; 
namespace Bank.Services.MessageTypes
{
    public class TransferMessage
    {
        public double Amount { get; set; }
        public int FromAccountNumber { get; set; }
        public int ToAccountNumber { get; set; }
        public String Reference { get; set; }
        public int orderId { get; set; }
    }
} 