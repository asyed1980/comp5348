﻿using System;
using VideoStore.Services.MessageTypes;

namespace Bank.Services.Interfaces
{
    public interface ITransferOutcomeNotificationService
    {
        void NotifyTransferOutcomeService(NotificationMessage msg);
    }
}
