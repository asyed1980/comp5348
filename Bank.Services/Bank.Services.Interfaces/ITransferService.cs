﻿using System.ServiceModel;
using Bank.Services.MessageTypes;

namespace Bank.Services.Interfaces
{
    [ServiceContract]
    public interface ITransferService
    {
        [OperationContract(IsOneWay = true)]
        void Transfer(TransferMessage msg);
    }
}
