﻿
using Bank.Services.MessageTypes;
using VideoStore.Services.MessageTypes;

namespace Bank.Business.Components.Interfaces
{
    public interface ITransferProvider
    {
        NotificationMessage Transfer(TransferMessage msg);   
    }
}
