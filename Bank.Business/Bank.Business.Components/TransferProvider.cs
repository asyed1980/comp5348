﻿using System;
using System.Linq;
using Bank.Business.Components.Interfaces;
using Bank.Business.Entities;
using System.Transactions;
using VideoStore.Services.MessageTypes;
using Bank.Services.Interfaces;
using VideoStore.Services.Interfaces;

namespace Bank.Business.Components
{
    public class TransferProvider : ITransferProvider
    {


        public NotificationMessage Transfer(Bank.Services.MessageTypes.TransferMessage msg)
        {
            
             NotificationMessage  nMsg = new NotificationMessage();
            nMsg.NotificationType = NotificationMessage.Type.Transfer;
            nMsg.orderRef = msg.Reference;

            using (TransactionScope lScope = new TransactionScope())
            using (BankEntityModelContainer lContainer = new BankEntityModelContainer())
            {
                try
                {

                    Console.WriteLine("Invoke transferring ${0} from Acct.{1} -=> to Acc.{2}     reference:{3} ", msg.Amount, msg.ToAccountNumber, msg.FromAccountNumber, msg.Reference);
                    Account lFromAcct = GetAccountFromNumber(msg.FromAccountNumber);
                    Account lToAcct = GetAccountFromNumber(msg.ToAccountNumber);
                    lFromAcct.Withdraw(msg.Amount);
                    lToAcct.Deposit(msg.Amount);
                    lContainer.Attach(lFromAcct);
                    lContainer.Attach(lToAcct);
                    lContainer.ObjectStateManager.ChangeObjectState(lFromAcct, System.Data.EntityState.Modified);
                    lContainer.ObjectStateManager.ChangeObjectState(lToAcct, System.Data.EntityState.Modified);
                    lContainer.SaveChanges();
                    lScope.Complete();
                    nMsg.NotificationStatus = NotificationMessage.Status.Success;
                    nMsg.Message = "Successful Money Transfer of $" + msg.Amount + " from Acct. " + msg.ToAccountNumber + " --> to Acct." + msg.FromAccountNumber + "     reference:" + msg.Reference;

                }
                catch (Exception lException)
                {
                    Console.WriteLine("Error occured while transferring money:  " + lException.Message);
                    //throw;                   
                    nMsg.NotificationStatus = NotificationMessage.Status.Failure;
                    nMsg.Message = "Error occured while transferring money:  " + lException.Message;
                    lScope.Dispose();
                }
                return nMsg;
            }


        }

        private Account GetAccountFromNumber(int accNum)
        {
            using (BankEntityModelContainer lContainer = new BankEntityModelContainer())
            {
                return lContainer.Accounts.Where((pAcct) => (pAcct.AccountNumber == accNum)).FirstOrDefault();
            }
        }
    }
}
