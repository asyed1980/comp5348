﻿
using System;
using Bank.Services.Interfaces;
using Bank.Services.MessageTypes;
using Bank.Business.Components.Interfaces;
using Bank.Business.Components;
using VideoStore.Services.MessageTypes;
using Bank.Services.NotificationService;

namespace Bank.Services
{
    public class TransferService : ITransferService
    {
        ITransferProvider provider=new TransferProvider();
        public void Transfer(TransferMessage msg)
        {
            
           NotificationMessage notificationMsg = provider.Transfer(msg);
            INotificationService noptificationService= NotificationServiceFactory.GetTransferOutcomeNotificationService();
            noptificationService.Notification(notificationMsg);
        }
    }
}
