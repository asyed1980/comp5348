﻿using Bank.Services.NotificationService;

namespace Bank.Services
{
    public class NotificationServiceFactory
    {
        private static INotificationService _INSTANCE = null;
        public static  INotificationService GetTransferOutcomeNotificationService()
        {
             
            if (_INSTANCE == null) _INSTANCE = new  NotificationServiceClient();
            return _INSTANCE;
        }
    }
}
