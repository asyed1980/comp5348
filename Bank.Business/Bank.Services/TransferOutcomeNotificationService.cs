﻿using Bank.Services.Interfaces;
using System;
using System.ServiceModel.Channels;
using System.ServiceModel;
using VideoStore.Services.Interfaces;
using VideoStore.Services.MessageTypes; 

namespace Bank.Services
{
    public class TransferOutcomeNotificationService : ITransferOutcomeNotificationService
    {
        public void NotifyTransferOutcomeService(string reference, TransferOutcome outcome)
        {

            Binding binding = new NetMsmqBinding();
            EndpointAddress address = new EndpointAddress("net.msmq.://localhost:9020");//its a local call so ok to have hard coded Que 
            INotifyTransferOutcome proxy = ChannelFactory<INotifyTransferOutcome>.CreateChannel(binding, address);
            using (proxy as IDisposable)
            {
                proxy.NotifyOperationOutcome(reference, outcome);
            }
        }
    }
}
