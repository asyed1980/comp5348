﻿
using VideoStore.Services.Interfaces;
using VideoStore.Business.Components.Interfaces;
using VideoStore.Services.MessageTypes;
using VideoStore.Services.BankTransferService;
using VideoStore.Services.EmailService;
using VideoStore.Services.DeliveyService;

using System.ServiceModel;
using System;
using System.Net;
using System.Configuration;
using System.IO;
using System.Web;

namespace VideoStore.Services
{
    public class OrderService : IOrderService
    {

        private IOrderProvider OrderProvider
        {
            get
            {
                return ServiceFactory.GetService<IOrderProvider>();
            }
        }

        IDeliveryService deliveryProxy = new DeliveryServiceClient();
        IEmailService emailProxy = new EmailServiceClient();
        ITransferService bankProxy = new TransferServiceClient();

        public void SubmitOrder(Order pOrder)
        {
            try
            {
                pOrder = OrderProvider.SubmitOrder(
                    MessageTypeConverter.Instance.Convert<
                    VideoStore.Services.MessageTypes.Order,
                    VideoStore.Business.Entities.Order>(pOrder)
                );// above method modifed to make internal changes ( \i.e with in this system


                TransferMessage message = new TransferMessage();
                UserService userServ = new UserService();
                User cust = pOrder.Customer;
                message.Amount = pOrder.Total;
                message.orderId = pOrder.Id;

                message.ToAccountNumber = RetrieveVideoStoreAccountNumber();
                message.FromAccountNumber = userServ.GetUserAccountNum(cust);
                message.Reference = pOrder.OrderNumber.ToString();
                bankProxy.Transfer(message);
                EmailService.EmailMessage msg = new EmailService.EmailMessage();
                msg.ToAddresses = cust.Email;
                msg.Message = "This is to notify that you order has been placed and is in process";
            }
            catch (VideoStore.Business.Entities.InsufficientStockException ise)
            {
                throw new FaultException<InsufficientStockFault>(
                    new InsufficientStockFault() { ItemName = ise.ItemName });
            }
        }

        public void PlaceRequestDeliveryForOrder(string reference)
        {
            Order pOrder = RetieveOrder(reference);
            DeliveryInfo dInfo = new DeliveryInfo();
            dInfo.OrderNumber = pOrder.OrderNumber.ToString();
            dInfo.SourceAddress = "Video Store Address";
            dInfo.DestinationAddress = pOrder.Customer.Address;
            dInfo.DeliveryNotificationAddress = pOrder.Customer.Address;
            dInfo.DeliveryIdentifier = pOrder.OrderNumber;

            deliveryProxy.SubmitDelivery(dInfo);
            EmailService.EmailMessage msg = new EmailService.EmailMessage();
            msg.ToAddresses = pOrder.Customer.Email;
            msg.Message = "This is to notify that you order has been confirmed and awaiting delivery";
            emailProxy.SendEmail(msg);

        }

        private Order RetieveOrder(string reference)
        {

            return MessageTypeConverter.Instance.Convert<
                       VideoStore.Business.Entities.Order, VideoStore.Services.MessageTypes.Order>(OrderProvider.RetrieveOrder(reference));
        }


        public int RetrieveVideoStoreAccountNumber()
        {
            return 123;
        }



        public void ProcessDeliveryNotificationSuccess(string reference)
        {
            Order order = RetieveOrder(reference);
            EmailService.EmailMessage msg = new EmailService.EmailMessage();
            msg.ToAddresses = order.Customer.Email;
            msg.Message = "This is to notify that you order has been delivered\r\nOrder Ref:" + reference;
            emailProxy.SendEmail(msg);

        }

        public void ProcessDeliveryNotificationFailure(string reference)
        {
            UserService userServ = new UserService();
            Order order = RetieveOrder(reference);
            EmailService.EmailMessage msg = new EmailService.EmailMessage();
            msg.ToAddresses = order.Customer.Email;
            msg.Message = "This is to notify that you order has not been delivered your money will be refunded\r\nOrder Ref:" + reference;
            emailProxy.SendEmail(msg);
            TransferMessage tmsg = new TransferMessage();
            tmsg.Amount = order.Total;
            tmsg.ToAccountNumber = userServ.GetUserAccountNum(order.Customer);
            tmsg.FromAccountNumber = RetrieveVideoStoreAccountNumber();
            tmsg.Reference = "-1 Refund";
            tmsg.orderId = -1;
            bankProxy.Transfer(tmsg);
            // FIX for bug from demo 
            forceCustomersToUpdatCahce();
        }

        public void ProcessBankNotificationFailure(string reference)
        {
            Order order = RetieveOrder(reference);
            EmailService.EmailMessage msg = new EmailService.EmailMessage();
            msg.ToAddresses = order.Customer.Email;
            msg.Message = "This is to notify that your order has been cancelled because of bank declined money transfer.\r\nOrder Ref:" + reference;
            emailProxy.SendEmail(msg);
            OrderProvider.ProcessBankNotificationFailure(reference);
            // FIX for bug from demo 
            forceCustomersToUpdatCahce();
        }

        private void forceCustomersToUpdatCahce()
        {
            try
            {
                var client = new WebClient();
                var appSettings = ConfigurationManager.AppSettings;
                //read from App copnf file in case not configured use default from localhost
                string refreshLink = appSettings["refreshLink"] ?? "http://localhost:1274/Store/ForceCatalogRefreshForAll";
                //  var content = client.DownloadString(refreshLink);//eat the output  
                HttpWebRequest request = WebRequest.Create(refreshLink.Trim()) as HttpWebRequest;
               // Console.WriteLine("URI = "+request.RequestUri);
                request.Method = "GET";
                request.KeepAlive = true;                
                request.Accept="text / html,application / xhtml + xml,application / xml; q = 0.9,image / webp,*/*;q=0.8";
                request.Headers.Add("Accept-Language", "en-US,en;q=0.8"); 
                request.UserAgent="Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36";
                
                //var p= System.Diagnostics.Process.Start(refreshLink);
                //p.Close();
                HttpWebResponse response = request.GetResponse() as HttpWebResponse;
                 
                StreamReader streamr = new StreamReader(response.GetResponseStream());
                string line = streamr.ReadToEnd();
               Console.WriteLine("Response  for role back forced update cache:  " + line);
            }
            catch (Exception e) {
                Console.WriteLine("Exception "+e.Message);
            }
        }

        public void ProcessBankNotificationSuccess(string reference)
        {
            Order order = RetieveOrder(reference);
            EmailService.EmailMessage msg = new EmailService.EmailMessage();
            msg.ToAddresses = order.Customer.Email;
            msg.Message = "This is to notify that your order payment was successful\r\nOrder Ref:" + reference;
            emailProxy.SendEmail(msg);
            OrderProvider.ProcessBankNotificationSuccess(reference);
            RetieveOrder(reference);
            PlaceRequestDeliveryForOrder(reference);
        }
    }
}
