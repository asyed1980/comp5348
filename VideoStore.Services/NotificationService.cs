﻿using System; 
using VideoStore.Services.Interfaces;
using VideoStore.Services.MessageTypes;

namespace VideoStore.Services
{
    class NotificationService : INotificationService
    {
        public void Notification(NotificationMessage msg)
        {

            IOrderService orderService=new OrderService();
            
            if (msg.NotificationType == NotificationMessage.Type.Delivery) {
                if (msg.orderRef.StartsWith("-1")) {
                    Console.WriteLine("Processed refund :: "+msg.Message);
                    return;
                }
                Console.WriteLine("Receive notification from Delivery Co.  for order "+msg.orderRef);
                if (msg.NotificationStatus==NotificationMessage.Status.Success) {
                    orderService.ProcessDeliveryNotificationSuccess(msg.orderRef);
                }
                else {
                    orderService.ProcessDeliveryNotificationFailure(msg.orderRef);
                }
            }
            else
            {//service.ProcessBankNotificationSuccess(msg.orderRef);
                if (msg.NotificationType==NotificationMessage.Type.Transfer) {
                    Console.WriteLine("Receive notification from Bank for order " + msg.orderRef);
                    if (msg.NotificationStatus == NotificationMessage.Status.Success)
                    {
                        orderService.ProcessBankNotificationSuccess(msg.orderRef);
                    }
                    else
                    {
                        orderService.ProcessBankNotificationFailure(msg.orderRef);
                    }
                }

            }
            
        }
    }
}
