﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DeliveryCo.Business.Components.Interfaces;
using System.Transactions;
using DeliveryCo.Business.Entities;
using static DeliveryCo.Services.DeliveryService;

namespace DeliveryCo.Business.Components
{
    public class DeliveryProvider : IDeliveryProvider
    {
        public void SubmitDelivery(DeliveryCo.Business.Entities.DeliveryInfo pDeliveryInfo)
        {
            using(TransactionScope lScope = new TransactionScope())
            using(DeliveryDataModelContainer lContainer = new DeliveryDataModelContainer())
            {
                Console.WriteLine("Delivery Submitted .. REF:"+pDeliveryInfo.OrderNumber);
                lContainer.DeliveryInfoes.AddObject(pDeliveryInfo);
                lContainer.SaveChanges(); 
                lScope.Complete();
            }
            
        }

        public void ScheduleDelivery(Guid pDeliveryInfo)
        {
            using (TransactionScope lScope = new TransactionScope())
            using (DeliveryDataModelContainer ctx = new DeliveryDataModelContainer())
            {
                DeliveryInfo dInfo= ctx.DeliveryInfoes.Where((d) => (d.DeliveryIdentifier == pDeliveryInfo)).FirstOrDefault();
                Console.WriteLine(" Delivering to " + dInfo.DestinationAddress+ " :: Order Ref: "+dInfo.OrderNumber);
                dInfo.Status = (int)DeliveryStatus.DELIVERED;
                ctx.SaveChanges(); // retirved from ctx no
                lScope.Complete();
            }           
        }
 
    }
}
