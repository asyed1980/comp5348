﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VideoStore.Services.Interfaces;
using VideoStore.Services.MessageTypes;
using VideoStore.WebClient.ClientModels;

namespace VideoStore.WebClient.ViewModels
{
    public class CatalogueViewModel
    {
        private static Boolean forceRefresh=false; 

        private SortedDictionary<int,Media> storeItems=null;

        private ICatalogueService CatalogueService
        {
            get
            {
                return  ServiceFactory.Instance.CatalogueService;
            }
        }


        public CatalogueViewModel(){
            
        }

        public CatalogueViewModel(SortedDictionary<int, Media> storeItemMap)
        {
            storeItems = storeItemMap; 
        }


        public SortedDictionary<int, Media> getStoreItemsAsMap()
        {

            if (storeItems == null)
            {
                storeItems = new SortedDictionary<int, Media>();
                List<Media> freshItemList = CatalogueService.GetMediaItems(0, Int32.MaxValue);
                foreach (Media media in freshItemList)
                {
                    storeItems.Add(media.Id, media);
                }
                return storeItems;
            }
            else
            {
                return storeItems;
            }
        }



        public List<Media> Items
        {
            get {
                if (storeItems == null) {
                     storeItems = new SortedDictionary<int, Media>();
                    List<Media> freshItemList = CatalogueService.GetMediaItems(0, Int32.MaxValue);
                    foreach (Media media in freshItemList) {
                        storeItems.Add(media.Id,media);
                    }
                }

                return storeItems.Values.ToList();

            }
        }
    }
}