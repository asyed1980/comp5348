﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Web;
using System.Web.Mvc;
using VideoStore.Business.Entities;
using VideoStore.Services.MessageTypes;
using VideoStore.WebClient.ClientModels;
using VideoStore.WebClient.ViewModels;

namespace VideoStore.WebClient.Controllers
{
    public class CartController : Controller
    {
       
        public ViewResult Index(Cart pCart, string pReturnUrl)
        {
            ViewData["returnUrl"] = pReturnUrl;
            ViewData["CurrentCategory"] = "Cart";
            return View(pCart);
        }

        public RedirectToRouteResult AddToCart(Cart pCart, int pMediaId, string pReturnUrl)
        { 
            SortedDictionary<int, Services.MessageTypes.Media> map = Session["_storeItems"] as SortedDictionary<int, Services.MessageTypes.Media>;

            //must have store items
            Services.MessageTypes.Media media;

            if (map == null)
            {
                throw new Exception("Add to Cart Access not known please from home >> ListMedia");
            }
            else {
                media = map[pMediaId];
            }

            if ((media.StockCount-1) < 0 ) {
                return RedirectToAction("InsufficientStock",new { pItem= media.Title});//?pItem="+ media.Title); 
            }

           pCart.AddItem(media, 1);
           media.StockCount-- ;

            return RedirectToAction("Index", new
            {
                pReturnUrl
            });
        }


        public RedirectToRouteResult RemoveFromCart(Cart pCart, int pMediaId, string pReturnUrl)
        {
            SortedDictionary<int, Services.MessageTypes.Media> map = Session["_storeItems"] as SortedDictionary<int, Services.MessageTypes.Media>;

            //must have store items
            Services.MessageTypes.Media media;

            if (map == null)
            {
                throw new Exception("Add to Cart Access not known please from home >> ListMedia");
            }
            else
            {
                media = map[pMediaId];
            }

           int totalRemoved =   pCart.RemoveLine(media);
            media.StockCount+= totalRemoved;
            return RedirectToAction("Index", new { pReturnUrl });
        }


        

        public ActionResult CheckOut(Cart pCart, UserCache pUser)
        {
            try
            {
                pCart.SubmitOrderAndClearCart(pUser);
                Session["_storeItems"]=null;
            }
            catch (Exception e)
            {
                pCart.Clear();
                pUser.UpdateUserCache();
                return RedirectToAction("ErrorPage");
            }
            return View(new CheckOutViewModel(pUser.Model));
        }

        public ActionResult ErrorPage()
        {
            return View();
        }

        public ViewResult Summary(Cart pCart)
        {
            return View(pCart);
        }

        public ActionResult InsufficientStock(String pItem)
        {
            return View(new InsufficientStockViewModel(pItem));
        }
        
    }
}