﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VideoStore.Services.MessageTypes;
using VideoStore.WebClient.ViewModels;

namespace VideoStore.WebClient.Controllers
{
   
    public class StoreController : Controller
    {



        //we want this to flag all Customers in system to update cache
        private static int referenceCacheVersion = 0;

        // GET: Store
        public ActionResult Index()
        {
            return View();
        }

      
        [AllowAnonymous]
        [HttpGet] 
        public ActionResult ForceCatalogRefreshForAll()
        {
            referenceCacheVersion++;
            var headers = String.Empty;
            foreach (var key in Request.Headers.AllKeys)
                headers += key + "=" + Request.Headers[key] + Environment.NewLine;
            
            return View();
        }

        public ActionResult ListMedia()
        {
            int cacheVersion = -1;
            SortedDictionary<int, Media> map;
            String cacheVersionStr = (Session["_cacheVersion"] as String);
            if (cacheVersionStr != null)  cacheVersion= Convert.ToInt32(cacheVersionStr);

            if (referenceCacheVersion>cacheVersion) {
                Session["_storeItems"] = null;
            } 
            map = Session["_storeItems"] as SortedDictionary<int, Media>;
            CatalogueViewModel model;
            if (map != null) { model = new CatalogueViewModel(map); }
            else{
                model = new CatalogueViewModel();
                Session["_storeItems"] =model.getStoreItemsAsMap();
                Session["_cacheVersion"] = referenceCacheVersion.ToString();
            }

            return View(model);
        }


    }

}