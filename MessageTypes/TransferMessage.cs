﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bank.Services.MessageTypes
{
    public class TransferMessage
    {
        public double pAmount { get; set; }
        public int pFromAcctNumber { get; set; }
        public int pToAcctNumber { get; set; }
        public String reference { get; set; }
    }
}
