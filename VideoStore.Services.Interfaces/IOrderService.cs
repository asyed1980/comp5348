﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using VideoStore.Services.MessageTypes;

namespace VideoStore.Services.Interfaces
{ 
    [ServiceContract]
    public interface IOrderService 
    { 
        [OperationContract]
        void SubmitOrder(Order pOrder);

        void PlaceRequestDeliveryForOrder(String reference);
        void ProcessBankNotificationFailure(String reference);
        void ProcessBankNotificationSuccess(String reference);
        void ProcessDeliveryNotificationSuccess(String reference);
        void ProcessDeliveryNotificationFailure(String reference);


    }
}
