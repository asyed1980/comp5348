﻿ 
using System.ServiceModel;
using VideoStore.Services.MessageTypes;

namespace VideoStore.Services.Interfaces
{ 
    [ServiceContract]
    public interface  INotificationService
    {
        [OperationContract (IsOneWay =true)]
        void Notification(NotificationMessage msg);
    }
}
